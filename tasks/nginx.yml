---
- name: Gather the package facts
  package_facts:
    manager: auto

- name: Get version of installed Nginx package
  set_fact:
    nginx_http2_standalone: "{{ ansible_facts.packages['nginx'][0].version is version('1.25', operator='>', strict=False) }}"
  when: "'nginx' in ansible_facts.packages"

- name: Nginx is higher than version 1.25
  debug:
    msg: "{{ nginx_http2_standalone }}"

- name: Create Nginx site folders
  file:
    path: "{{ item }}"
    state: directory
  loop:
    - /etc/nginx/sites-available
    - /etc/nginx/sites-enabled

- name: Add configuration files
  block:
    - name: Add NGINX server config
      template:
        src: nginx/server.conf.j2
        dest: /etc/nginx/sites-available/{{ project_name }}
        backup: true
      register: server_conf
      notify: Restart NGINX
    - name: Validate NGINX configuration
      command: nginx -t
      changed_when: false
  rescue:
    - name: Restore server config from backup
      copy:
        remote_src: "{{ remote_src }}"
        src: "{{ server_conf.backup_file }}"
        dest: /etc/nginx/sites-available/{{ project_name }}
    - name: Remove backup file
      file:
        path: "{{ server_conf.backup_file }}"
        state: absent
    - name: Abort
      fail:
        msg: NGINX config not valid

- name: Create symbolic link in sites-enabled
  file:
    src: ../sites-available/{{ project_name }}
    dest: /etc/nginx/sites-enabled/{{ project_name }}
    owner: root
    group: root
    state: link

- name: Add mappings to /etc/hosts
  blockinfile:
    path: /etc/hosts
    block: |
      127.0.0.1 {{ project_domains | join(' ') }}
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ project_name }}"
  when:
    - inventory_hostname == "localhost"